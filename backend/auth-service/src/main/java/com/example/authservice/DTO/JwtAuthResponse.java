package com.example.authservice.DTO;

import lombok.Data;

@Data
public class JwtAuthResponse {

    private String token;
    private String refreshToken;
}
