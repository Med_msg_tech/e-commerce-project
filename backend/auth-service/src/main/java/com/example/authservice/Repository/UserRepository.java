package com.example.authservice.Repository;


import com.example.authservice.Entity.Role;
import com.example.authservice.Entity.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends MongoRepository<User, String> {

    public User findByEmail(String email);
    public User findByRole(Role role);

    public User findUserByEmailAndPassword(String email, String password);
}
