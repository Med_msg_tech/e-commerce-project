package com.example.authservice.Controllers;


import com.example.authservice.DTO.JwtAuthResponse;
import com.example.authservice.DTO.RefreshToken;
import com.example.authservice.DTO.SignInRequest;
import com.example.authservice.DTO.SignUpRequest;
import com.example.authservice.Entity.User;
import com.example.authservice.Service.AuthService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin("*")
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/authentication")
public class AuthController {

    private final AuthService authService;


    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ResponseEntity<String> get(){
        return ResponseEntity.ok("auth Route working !");
    }
    @PostMapping("/create")
    public ResponseEntity<User> createUser(@RequestBody SignUpRequest signUpRequest){
        return ResponseEntity.ok(authService.signup(signUpRequest));
    }

    @PostMapping("/signin")
    public ResponseEntity<JwtAuthResponse> signin(@RequestBody SignInRequest signInRequest){
        return ResponseEntity.ok(authService.signin(signInRequest));
    }

    @GetMapping("/refresh")
    public ResponseEntity<JwtAuthResponse> refresh(@RequestBody RefreshToken refreshToken){
        return ResponseEntity.ok(authService.refreshToken(refreshToken));
    }
}
