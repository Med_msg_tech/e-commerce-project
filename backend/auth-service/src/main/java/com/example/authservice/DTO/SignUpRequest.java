package com.example.authservice.DTO;

import lombok.Data;

@Data
public class SignUpRequest {

    private String email;
    private String password;
}
