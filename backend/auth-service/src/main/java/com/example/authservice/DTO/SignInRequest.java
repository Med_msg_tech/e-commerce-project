package com.example.authservice.DTO;

import lombok.Data;

@Data
public class SignInRequest {

    private String email;
    private String password;
}
