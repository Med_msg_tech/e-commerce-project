package com.example.authservice.Service;

import com.example.authservice.DTO.JwtAuthResponse;
import com.example.authservice.DTO.RefreshToken;
import com.example.authservice.DTO.SignInRequest;
import com.example.authservice.DTO.SignUpRequest;
import com.example.authservice.Entity.Role;
import com.example.authservice.Entity.User;
import com.example.authservice.Repository.UserRepository;
import com.example.authservice.Service.Interfaces.JWTService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashMap;

@Service
@RequiredArgsConstructor
public class AuthService {

    private final UserRepository userRepository;
    private final UserService userService;

    @Autowired
    private AuthenticationManager authenticationManager;

    private final PasswordEncoder passwordEncoder;
    private final JWTService jwtService;

    public User signup(SignUpRequest signUpRequest){
        User user = new User();
        user.setEmail(signUpRequest.getEmail());
        user.setPassword(passwordEncoder.encode(signUpRequest.getPassword()));
        user.setRole(
                Role.USER
        );
        return userRepository.insert(user);
    }

    public JwtAuthResponse signin(SignInRequest signInRequest){
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(signInRequest.getEmail(), signInRequest.getPassword()));
        User user =  userRepository.findByEmail(signInRequest.getEmail());
        if (user == null){
            throw new IllegalArgumentException("Invalid email or password");
        }
        String jwt = jwtService.generateToken(new HashMap<>(), user);
        String refreshedToken = jwtService.refreshToken(new HashMap<>(), user);

        JwtAuthResponse jwtAuthResponse = new JwtAuthResponse();
        jwtAuthResponse.setToken(jwt);
        jwtAuthResponse.setRefreshToken(refreshedToken);

        return jwtAuthResponse;
    }

    public JwtAuthResponse refreshToken(RefreshToken token){
        String userEmail = jwtService.ExtractUserName(token.getRefreshedToken());
        User user = userRepository.findByEmail(userEmail);

        if(jwtService.isTokenValid(token.getRefreshedToken(), user)){
            String jwt = jwtService.generateToken(new HashMap<>(), user);
            JwtAuthResponse jwtAuthResponse = new JwtAuthResponse();

            jwtAuthResponse.setToken(jwt);
            jwtAuthResponse.setRefreshToken(token.getRefreshedToken());
            return jwtAuthResponse;
        }
        return null;

    }
}
