package com.example.authservice.Controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin("*")
@RestController
@RequestMapping("/api/v1/admin")
public class adminController {

    @GetMapping("/")
    public ResponseEntity<String> get(){
        return ResponseEntity.ok("Hi Admin");
    }
}
