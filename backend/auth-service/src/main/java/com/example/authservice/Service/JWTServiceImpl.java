package com.example.authservice.Service;

import com.example.authservice.Service.Interfaces.JWTService;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import javax.crypto.SecretKey;
import java.security.Key;
import java.util.*;
import java.util.function.Function;

@Service
public class JWTServiceImpl  implements JWTService {


    @Value("@secret_key")
    private String s_key;

    private static final SecretKey key = Keys.secretKeyFor(SignatureAlgorithm.HS512);

    static {
        System.out.println(key.getFormat().toString());
    }
    public String ExtractUserName(String token){
        return extractClaim(token, Claims::getSubject);
    }
    @Override
    public String generateToken(Map<String , Object> extraClaims, UserDetails userDetails){
        return Jwts.builder()
                .setClaims(extraClaims)
                .setSubject(userDetails.getUsername())
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + 1000*60*24))
                .signWith(getSiginKey(), SignatureAlgorithm.HS256)
                .compact();
    }

    @Override
    public String refreshToken(Map<String, Object> extractClaims, UserDetails userDetails){
        return Jwts.builder().setClaims(extractClaims).setSubject(userDetails.getUsername())
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + 1000*60*24*7))
                .signWith(getSiginKey(), SignatureAlgorithm.HS256)
                .compact();
    }



    private <T> T extractClaim(String token, Function<Claims, T> ClaimsResolvers){
        final Claims claims = extractAllClaims(token);
        return ClaimsResolvers.apply(claims);
    }

    private Key getSiginKey(){
        return Keys.hmacShaKeyFor(Decoders.BASE64.decode("404E635266556A586E3272357538782F413F4428472B4B6250645367566B5970"));
    };

    private Claims extractAllClaims(String token){
        return Jwts.parserBuilder().setSigningKey(getSiginKey()).build().parseClaimsJws(token).getBody();
    }


    @Override
    public boolean isTokenValid(String token, UserDetails userDetails){
        return (this.ExtractUserName(token).equals(userDetails.getUsername()) && !isTokenExpired(token));
    }

    private boolean isTokenExpired(String token) {
        return extractClaim(token, Claims::getExpiration).before(new Date());
    }


}
