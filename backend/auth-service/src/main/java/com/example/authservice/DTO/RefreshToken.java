package com.example.authservice.DTO;

import lombok.Data;

@Data
public class RefreshToken {

    private String refreshedToken;
}
