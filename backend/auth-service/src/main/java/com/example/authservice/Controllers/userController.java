package com.example.authservice.Controllers;


import com.example.authservice.Entity.User;
import com.example.authservice.Service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin("*")
@RestController
@RequestMapping("/api/v1/user")
public class userController {

    @Autowired
    private UserService userService;

    @Value("${db.username}")
    private String username;

    @GetMapping("/")
    public ResponseEntity<String> get(){
        return ResponseEntity.ok("Hi User");
    }
     @GetMapping("/test")
    public String test(){
         return "User controller Working! " + this.username;
     }

     @PostMapping("/createUser")
    public ResponseEntity<User> create_user(@RequestBody User user){
         return ResponseEntity.ok(userService.createUser(user));
     }
}
