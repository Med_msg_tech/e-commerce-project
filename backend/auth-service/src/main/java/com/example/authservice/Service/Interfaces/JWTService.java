package com.example.authservice.Service.Interfaces;


import org.springframework.security.core.userdetails.UserDetails;

import java.util.Map;

public interface JWTService {

    String ExtractUserName(String token);

    String generateToken(Map<String , Object> extraClaims, UserDetails userDetails);
    String refreshToken(Map<String, Object> extractClaims, UserDetails userDetails);

    boolean isTokenValid(String token, UserDetails userDetails);
}
