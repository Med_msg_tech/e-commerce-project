package com.example.apigateway.Controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
public class HomeController {

    @GetMapping("/")
    public String Home(){
        return "Hello from Home, Insecured !";
    }

    @GetMapping("/secured")
    public String HomeSecured(){
        return "welcome back";
    }
}
