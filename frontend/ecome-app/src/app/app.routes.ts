import { Routes } from '@angular/router';
import { AuthComponent } from './auth/auth.component';
import { LoginComponent } from './components/login/login.component';
import { LandingComponent } from './components/landing/landing.component';
import { RegisterComponent } from './components/register/register.component';
import { HomeComponent } from './components/home/home.component';
import { AuthGuard } from './services/guard.service';

export const routes: Routes = [
    {
        path : "",
        component: LandingComponent
    },
    {
        path : "auth",
        component : AuthComponent
    },
    {
        path : "login",
        component : LoginComponent
    },
    {
        path : "register",
        component : RegisterComponent
    },
    {
        path : "home",
        component : HomeComponent,
        canActivate : [AuthGuard]
    }
];
