import { CommonModule } from '@angular/common';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { Component, NgZone } from '@angular/core';
import { FormControl, FormsModule, NgModel } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  standalone: true,
  imports: [CommonModule, FormsModule, HttpClientModule],
  templateUrl: './login.component.html',
  styleUrl: './login.component.css'
})
export class LoginComponent {
  public email !: string;
  public password!: string;

  constructor(private http : HttpClient, private ngZone: NgZone, private router: Router){
    
  }

  
  login(email : string, password: string){
    // validate email with regex
    console.log(email, password)
    // make http call
    this.http.post(`http://localhost:8181/api/v1/authentication/signin`,{
      email : this.email,
      password : this.password
    }, {
      "headers" : {
        "content-type" : "application/json",
        'Access-Control-Allow-Origin': '*'
      }
    }).subscribe((res : any)=>{
      // get the token 
      let token = res.token;
      console.log(token)
      this.ngZone.run(()=>{
        localStorage.setItem("token", token)
      })
      this.email = this.password = "",
      this.router.navigate(['/home'])
    })
  }
}
