import { HttpClient } from "@angular/common/http"



export class ApiService{

    private apiAdresse : string = "localhost"
    private gateway : string = `http://${this.apiAdresse}:8080`

    constructor(private http: HttpClient){

    }

    get(endpoint :string){
        return this.http.get(this.gateway+endpoint, {
            
        })
    }
}