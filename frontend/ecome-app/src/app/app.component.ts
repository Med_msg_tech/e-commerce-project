import { Component } from '@angular/core';
import { LandingComponent } from "./components/landing/landing.component";
import { HeaderComponent } from "./components/header/header.component";
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

@Component({
    selector: 'app-root',
    standalone: true,
    templateUrl: './app.component.html',
    styleUrl: './app.component.css',
    imports: [LandingComponent, HeaderComponent, CommonModule, RouterModule]
})
export class AppComponent {
  title = 'ecome-app';
}
